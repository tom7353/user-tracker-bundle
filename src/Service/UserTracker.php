<?php
/**
 * MIT License
 *
 * Copyright (c) 2019 Tom Janke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace TomJanke\UserTracker\Service;


use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Throwable;
use TomJanke\UserTracker\Entity\PageView;

class UserTracker
{

  /** @var EntityManagerInterface */
  private $entityManager;

  /** @var IpHelper */
  private $ipHelper;

  /** @var LoggerInterface */
  private $logger;

  public function __construct(EntityManagerInterface $entityManager, IpHelper $ipHelper, LoggerInterface $logger) {
    $this->entityManager = $entityManager;
    $this->ipHelper = $ipHelper;
    $this->logger = $logger;
  }

  public function onPageView(Request $request): void {

    try {

      $ip = $this->ipHelper->anonymizeIp($request->getClientIp());
      $route = $request->attributes->get("_route");
      $date = (new DateTime())->setTime(0, 0, 0, 0);

      $pageView = $this->entityManager->getRepository(PageView::class)->findOneBy(["route" => $route, "ip" => $ip, "created" => $date]) ?? new PageView($route, $ip);

      $this->entityManager->persist($pageView->incrementCount());
      $this->entityManager->flush();

    } catch (Throwable $t) {
      $this->logger->error("UserTracker: Could not track request.", [
        get_class($t),
        $t->getMessage()]);
    }

  }

  /**
   * Returns the total number of page views in an associative array with the date string as a key.
   * <p>
   * <tt>
   * array (<br>
   * '2019-01-01' => 32,<br>
   * '2019-01-02' => 24,<br>
   * )
   * </tt>
   *
   * @param int $days
   * @return array
   */
  public function getPageViewsPerDay(int $days = 30): array {

    $result = $this->entityManager->createQueryBuilder()
      ->select("page_view.created date")
      ->addSelect("coalesce(sum(page_view.count), 0) page_views")
      ->from(PageView::class, "page_view")
      ->groupBy("page_view.created")
      ->orderBy("page_view.created", "asc")
      ->setMaxResults($days)
      ->getQuery()
      ->getResult();

    $data = [];
    foreach ($result as $entry)
      $data[$entry["date"]->format("Y-m-d")] = intval($entry["page_views"]);

    return $data;
  }

  /**
   * Returns the total number of users in an associative array with the date string as a key.
   * <p>
   * <tt>
   * array (<br>
   * '2019-01-01' => 32,<br>
   * '2019-01-02' => 24,<br>
   * )
   * </tt>
   *
   * @param int $days
   * @return array
   */
  public function getUsersPerDay(int $days = 30): array {

    $result = $this->entityManager->createQueryBuilder()
      ->select("page_view.created date")
      ->addSelect("coalesce(count(distinct page_view.ip), 0) users")
      ->from(PageView::class, "page_view")
      ->groupBy("page_view.created")
      ->orderBy("page_view.created", "asc")
      ->setMaxResults($days)
      ->getQuery()
      ->getResult();

    $data = [];
    foreach ($result as $entry)
      $data[$entry["date"]->format("Y-m-d")] = intval($entry["users"]);

    return $data;
  }

}