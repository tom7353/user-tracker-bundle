<?php
/**
 * MIT License
 *
 * Copyright (c) 2019 Tom Janke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace TomJanke\UserTracker\Entity;


use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="user_tracker_page_view", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_created_route_ip", columns={"created", "route", "ip"})})
 * @ORM\HasLifecycleCallbacks()
 * @package App\Entity
 */
class PageView
{


  /**
   * @var int|null
   * @ORM\Id()
   * @ORM\Column(type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var DateTime
   * @ORM\Column(type="date")
   */
  private $created;

  /**
   * @var string
   * @ORM\Column(type="string", length=63)
   */
  private $route;

  /**
   * @var string
   * @ORM\Column(type="string", length=63)
   */
  private $ip;

  /**
   * @var int
   * @ORM\Column(type="integer")
   */
  private $count = 0;

  /**
   * PageView constructor.
   * @param string $route
   * @param string $ip
   */
  public function __construct(string $route, string $ip) {
    $this->route = $route;
    $this->ip = $ip;
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function incrementCount(): self {
    $this->count++;

    return $this;
  }

  public function getRoute(): string {
    return $this->route;
  }

  public function getIp(): string {
    return $this->ip;
  }

  public function getCount(): int {
    return $this->count;
  }

  /**
   * @ORM\PrePersist()
   * @ORM\PreUpdate()
   */
  public function updateCreated() {
    if (!$this->created || $this->created === null)
      $this->created = new DateTime();
  }

  public function getCreated(): DateTime {
    return $this->created;
  }

}