# UserTrackerBundle

GDPR friendly tracking of your Symfony web applications users.

---

## Installation

```bash
composer require tomjanke/user-tracker-bundle
```

Be sure to also update your database, as a new table `user_tracker_page_view` will be created.
If you're using Doctrine migrations, you'd the following:
```bash
php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate
```

---

## Documentation

### Exposed services

#### IpHelper

Class: `TomJanke\UserTracker\Service\IpHelper`

##### anonymizeIp(string): string

Anonymizes the given ip by replacing the last part of it.

```php
/** @var IpHelper $ipHelper */

$anonymizedIp = $ipHelper->anonymizeIp("127.0.0.1");
// $anonymizedIp will be '127.0.0.X'
echo $anonymizedIp;
```

#### UserTracker

Class: `TomJanke\UserTracker\Service\UserTracker`

##### getPageViewsPerDay(int): array

Returns the total number of page views in an associative array with the date string as a key.

```php
/** @var UserTracker $userTracker */

$pageViewsPerDay = $userTracker->getPageViewsPerDay(10);
// $pageViewsPerDay will contain the total page view count for the last 10 days
```

##### getUsersPerDay(int): array

Returns the total number of users in an associative array with the date string as a key.

```php
/** @var UserTracker $userTracker */

$usersPerDay = $userTracker->getUsersPerDay(10);
// $usersPerDay will contain the unique user count for the last 10 days
```